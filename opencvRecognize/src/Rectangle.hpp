#ifndef RECTANGLE_HPP_
#define RECTANGLE_HPP_

#include <opencv2/core/core.hpp>
using namespace cv;


class Rectangle {
public:
	Rectangle(Size size);
	Rectangle(Rectangle& copy);

	Size getSize() const;
	void drawOnImage(Point drawPoint, Mat image, Scalar color);
	void drawOnImage(Point drawPoint, Mat image);

private:
	Size size;
};


#endif /* RECTANGLE_HPP_ */
