#ifndef COMPARABLE_HPP_
#define COMPARABLE_HPP_

template <typename T>
class Comparable {

public:
	Comparable(T data, int compareValue);
	Comparable(const Comparable<T>&);

	T getData() const;
	int getCompareValue() const;

	bool operator>=(const Comparable<T>&) const;
	bool operator>(const Comparable<T>&) const;
	Comparable<T>& operator=(const Comparable<T>&);

private:
	T data;
	int compareValue;

};

template <typename T>
Comparable<T>::Comparable(T data, int compareValue) {
	this->compareValue = compareValue;
	this->data = data;
}

template <typename T>
Comparable<T>::Comparable(const Comparable<T>& comparable) {
	this->compareValue = comparable.getCompareValue();
	this->data = comparable.getData();
}

template <typename T>
T Comparable<T>::getData() const {
	return data;
}

template <typename T>
int Comparable<T>::getCompareValue() const {
	return compareValue;
}

template <typename T>
bool Comparable<T>::operator>=(const Comparable<T>& comparable) const {
	if (this->compareValue >= comparable.getCompareValue())
		return true;

	return false;
}

template <typename T>
bool Comparable<T>::operator>(const Comparable<T>& comparable) const {
	if (this->compareValue > comparable.getCompareValue())
		return true;

	return false;
}

template <typename T>
Comparable<T>& Comparable<T>::operator=(const Comparable<T>& comparable) {
	if (this != &comparable) {
		this->data = comparable.getData();
		this->compareValue = comparable.getCompareValue();
	}

	return (*this);
}

#endif /* COMPARABLE_HPP_ */
