#ifndef FILEEXCEPTION_HPP_
#define FILEEXCEPTION_HPP_

#include <exception>
#include <cstring>
using namespace std;


class FileException : public exception {

public:
	FileException(const char* file) {
		strcpy(error, "Impossibile aprire/creare il file ");
		strcat(error, file);
	}

	const char* what() const throw() {
		return error;
	}

private:
	char error[300];

};


#endif /* FILEEXCEPTION_HPP_ */
