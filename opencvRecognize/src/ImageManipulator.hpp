#ifndef IMAGEMANIPULATOR_HPP_
#define IMAGEMANIPULATOR_HPP_
#include <string>
#include <vector>
#include <opencv2/core/core.hpp>
#include "Rectangle.hpp"
#include "Student.hpp"
#include "Image.hpp"
#include "FileException.hpp"
#include "ContoursException.hpp"
#include "SortAlgorithms.hpp"
#include "StudentQuantityException.hpp"
#include <iostream>
using namespace std;
using namespace cv;

// SE IMPOSTATE AD 1 PERMETTONO DI VISUALIZZARE LE IMMAGINI DURANTE LA CREAZIONE
#define ANALYSIS_DEBUG 1
#define CREATING_DEBUG 1

class ImageManipulator {
public:
	ImageManipulator();
	~ImageManipulator();

	void createImage(string dataFilePath, string pathToCreate) throw (FileException, StudentQuantityException);
	void analyzeImage(Image anImage, string resultsFile) throw(FileException);

	Image alignImage(string originalPath, string toAlignPath, unsigned dilateErodeIteration) throw (ContoursException, FileException);
	vector<Point2f> getDocumentPointsCoordinates() const;

private:
	void getDataFromFile(string file, string& course, string& data, vector<Student>& students) throw (FileException, StudentQuantityException);
	Image warpImagePerspective(vector<Point2f>& rotatedPoints,
				vector<Point2f>& originalPoints, Image& rotatedImage);
	void drawBlackSquare(Size delta, Mat image);
	void writeResults(string file, vector<bool> presences) const throw(FileException);

	static const int IMAGE_WIDTH = 1000;
	static const int IMAGE_HEIGHT = 750;

	static const int RECTANGLE_HEIGHT = 40;
	static const int ID_WIDTH = 100;
	static const int CREDENTIALS_WIDTH = 300;
	static const int SIGNATURE_WIDTH = 400;

	static const int TABLE_UPEDGE_GAP = 120;
	static const int COURSENAME_UPEDGE_GAP = TABLE_UPEDGE_GAP - 90;
	static const int DATE_UPEDGE_GAP = TABLE_UPEDGE_GAP - 60;
	static const int TABLESIGNATURE_UPEDGE_GAP = TABLE_UPEDGE_GAP - 10;

	static const int ALIGN_TEXT_ROW = 20;
	static const int ALIGN_TEXT_COL = 25;

	static const int BLACKSQUARE_DIM = 50;

	static const int LEFT_EDGE_GAP = (IMAGE_WIDTH - (ID_WIDTH + CREDENTIALS_WIDTH + SIGNATURE_WIDTH)) / 2;

	static const int BLACK_PIXEL_TOLERANCE = 300;


	vector<Point2f> documentPointsCoordinates;
	Size nameSurnameSize;
	Size idSize;
	Size signatureSize;
	Rectangle* nameSurnameRectangle;
	Rectangle* idRectangle;
	Rectangle* signatureRectangle;
};


#endif /* IMAGEMANIPULATOR_HPP_ */
