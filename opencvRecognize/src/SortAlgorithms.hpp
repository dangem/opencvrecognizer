#ifndef SORTALGORITHM_HPP_
#define SORTALGORITHM_HPP_

#include <vector>
#include <climits>
#include "Square.hpp"
#include "Comparable.hpp"
using namespace std;

class SortAlgorithms {

public:
	static void sortSquares(vector<Square*>&);
	static void sortInternalSquarePoints(Square*);
	static void sortInternalSquaresPoints(vector<Square*>&);
	static vector<Square*> extrapulateSquareContours(vector<vector<Point2f> >);

	template <typename T>
	static void bubbleSort(vector<Comparable<T> >&);

private:
	SortAlgorithms();

};

#endif /* SORTALGORITHM_HPP_ */
