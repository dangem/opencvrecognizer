#include "ImageManipulator.hpp"
#include "Colours.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <fstream>
using namespace std;
using namespace cv;

ImageManipulator::ImageManipulator() {
	this->idSize = Size(ID_WIDTH, RECTANGLE_HEIGHT);
	this->nameSurnameSize = Size(CREDENTIALS_WIDTH, RECTANGLE_HEIGHT);
	this->signatureSize = Size(SIGNATURE_WIDTH, RECTANGLE_HEIGHT);
	this->signatureRectangle = new Rectangle(this->signatureSize);
	this->nameSurnameRectangle = new Rectangle(this->nameSurnameSize);
	this->idRectangle = new Rectangle(this->idSize);
}

ImageManipulator::~ImageManipulator() {
	delete this->signatureRectangle;
	delete this->nameSurnameRectangle;
	delete this->idRectangle;
}

void ImageManipulator::getDataFromFile(string file, string& course, string& date, vector<Student>& students)
	throw (FileException, StudentQuantityException) {
	ifstream inputStream(file.data(), ios::in);

	if (inputStream.is_open() == false)
		throw FileException(file.data());

	getline(inputStream, course);
	getline(inputStream, date);
	string studentNumber;
	getline(inputStream, studentNumber);

	int integerStudentNumber = atoi(studentNumber.data());
	for (int i = 0; i < integerStudentNumber; i++) {
		string studentTemp;
		getline(inputStream, studentTemp);
		stringstream stream(studentTemp);
		string idField;
		string credentialsField;
		getline(stream, idField, ' ');
		string ignore;
		getline(stream, ignore, ' ');
		getline(stream, credentialsField, '\n');
		students.push_back(Student(credentialsField, idField));
	}

	// IL NUMERO MINIMO DI STUDENTE DA IMMETTERE NEL FOGLIO DEVE ESSERE >= 3 ALTRIMENTI
	// LA SCANSIONE NON FUNZIONERA'
	if (students.size() < 3) {
		inputStream.close();
		throw StudentQuantityException();
	}

	inputStream.close();
}

void ImageManipulator::createImage(string dataFilePath, string pathToCreate) throw (FileException, StudentQuantityException) {
	string date;
	string courseName;
	vector<Student> students;
	getDataFromFile(dataFilePath, courseName, date, students);

	Image anImage(Mat(Size(IMAGE_WIDTH, IMAGE_HEIGHT), CV_32FC3), "creatingImage");
	Mat image = anImage.getImage();
	image.setTo(WHITE);

	putText(image, courseName, Point(LEFT_EDGE_GAP, COURSENAME_UPEDGE_GAP),
			FONT_HERSHEY_COMPLEX_SMALL, 1, BLACK, 1, 0, false);

	putText(image, date, Point(LEFT_EDGE_GAP, DATE_UPEDGE_GAP),
			FONT_HERSHEY_COMPLEX_SMALL, 1, BLACK, 1, 0, false);

	putText(image, "Matricola", Point(LEFT_EDGE_GAP, TABLESIGNATURE_UPEDGE_GAP),
			FONT_HERSHEY_COMPLEX_SMALL, 0.7, BLACK, 1, 0, false);

	putText(image, "Cognome Nome",
			Point(LEFT_EDGE_GAP + ID_WIDTH, TABLESIGNATURE_UPEDGE_GAP),
			FONT_HERSHEY_COMPLEX_SMALL, 0.7, BLACK, 1, 0, false);

	putText(image, "Firma",
			Point(LEFT_EDGE_GAP + ID_WIDTH + CREDENTIALS_WIDTH,
					TABLESIGNATURE_UPEDGE_GAP), FONT_HERSHEY_COMPLEX_SMALL, 0.7,
					BLACK, 1, 0, false);

	for (unsigned i = 0; i < students.size(); i++) {
		idRectangle->drawOnImage(
				Point(LEFT_EDGE_GAP, TABLE_UPEDGE_GAP + i * RECTANGLE_HEIGHT),
				image);
		nameSurnameRectangle->drawOnImage(
				Point(LEFT_EDGE_GAP + ID_WIDTH,
						TABLE_UPEDGE_GAP + i * RECTANGLE_HEIGHT), image);
		signatureRectangle->drawOnImage(
				Point(LEFT_EDGE_GAP + ID_WIDTH + CREDENTIALS_WIDTH,
						TABLE_UPEDGE_GAP + i * RECTANGLE_HEIGHT), image);
		putText(image, students[i].getID(),
				Point(LEFT_EDGE_GAP + ALIGN_TEXT_ROW,
						TABLE_UPEDGE_GAP + i * RECTANGLE_HEIGHT
						+ ALIGN_TEXT_COL), FONT_HERSHEY_COMPLEX_SMALL,
						0.8, BLACK, 1, 0, false);
		putText(image, students[i].getCredentials(),
				Point(LEFT_EDGE_GAP + ID_WIDTH + ALIGN_TEXT_ROW,
						TABLE_UPEDGE_GAP + i * RECTANGLE_HEIGHT
						+ ALIGN_TEXT_COL), FONT_HERSHEY_COMPLEX_SMALL,
						0.8, BLACK, 1, 0, false);
	}

	drawBlackSquare(Size(TABLE_UPEDGE_GAP, LEFT_EDGE_GAP - BLACKSQUARE_DIM),
			image);
	drawBlackSquare(
			Size(TABLE_UPEDGE_GAP,
					LEFT_EDGE_GAP + ID_WIDTH + CREDENTIALS_WIDTH
					+ SIGNATURE_WIDTH), image);
	drawBlackSquare(
			Size(
					TABLE_UPEDGE_GAP + (students.size() - 1) * RECTANGLE_HEIGHT
					- (BLACKSQUARE_DIM - RECTANGLE_HEIGHT),
					LEFT_EDGE_GAP - BLACKSQUARE_DIM), image);
	drawBlackSquare(
			Size(
					TABLE_UPEDGE_GAP + (students.size() - 1) * RECTANGLE_HEIGHT
					- (BLACKSQUARE_DIM - RECTANGLE_HEIGHT),
					LEFT_EDGE_GAP + ID_WIDTH + CREDENTIALS_WIDTH
					+ SIGNATURE_WIDTH), image);

#if CREATING_DEBUG == 1
	anImage.displayAndWait();
#endif

	anImage.saveTo(pathToCreate);
}

void ImageManipulator::drawBlackSquare(Size delta, Mat image) {
	Point squarePoints[1][4];
	squarePoints[0][0] = Point(delta.height, delta.width);
	squarePoints[0][1] = Point(delta.height, BLACKSQUARE_DIM + delta.width);
	squarePoints[0][2] = Point(BLACKSQUARE_DIM + delta.height,
			BLACKSQUARE_DIM + delta.width);
	squarePoints[0][3] = Point(BLACKSQUARE_DIM + delta.height, delta.width);
	const Point* points[1] = { squarePoints[0] };
	int nPoints[] = { 4 };

	fillPoly(image, points, nPoints, 1, BLACK);
}

void ImageManipulator::analyzeImage(Image anImage, string resultsFile) throw(FileException) {
	anImage.binaryTransform(190);
	// SI PUO' RICONVERTIRE L'IMMAGINE IN COLORI SENZA PERDERE INFORMAZIONE
	Mat image = anImage.getImage();

	cvtColor(image, image, CV_GRAY2RGB);
	// QUESTO PERCHE' PROBABILMENTE IL cvtColor FA UNA COPIA DELLA MATRICE CHE GLI VIENE PASSATA
	anImage.setImage(image);

	int startCol = documentPointsCoordinates[0].x + ID_WIDTH + CREDENTIALS_WIDTH;
	int startRow = documentPointsCoordinates[0].y;
	int width = SIGNATURE_WIDTH;
	int height = RECTANGLE_HEIGHT;

	int rowUpTolerance = 7;
	int rowDownTolerance = 5;
	int columnLeftTolerance = 5;
	int columnRightTolerance = 5;


	// CALCOLA IL NUMERO DI RETTANGOLI A PARTIRE DALLE COORDINATE DEI PUNTI E DALL'ALTEZZA DELLO STESSO RETTANGOLO
	unsigned rectNumber = (documentPointsCoordinates[3].y - documentPointsCoordinates[0].y) / RECTANGLE_HEIGHT;
	int blackPix;

	vector<bool> presences;

	for (unsigned i = 0; i < rectNumber; i++) {
		blackPix = 0;
		for (unsigned j = startRow + (height * i) + rowUpTolerance;	j < startRow + (height * (i + 1)) - rowDownTolerance; j++)
			for (int k = startCol + columnLeftTolerance; k < startCol + width - columnRightTolerance; k++)
				if (image.at<Vec3b>(j, k)[0] == 0 && image.at<Vec3b>(j, k)[1] == 0 && image.at<Vec3b>(j, k)[2] == 0)
					++blackPix;

		stringstream ss;
		ss << blackPix;
		string blackPixelString = ss.str();
		// I VALORI 35 E 25 ALLINEANO SEMPLICEMENTE IL TESTO NELLA CASELLA
		putText(image, blackPixelString, Point(startCol - 35, startRow + (height * i) + 25),
				FONT_HERSHEY_PLAIN, 1, RED, 1, 0, false);

		if (blackPix > BLACK_PIXEL_TOLERANCE)
			presences.push_back(true);
		else
			presences.push_back(false);

		rectangle(image, Point(startCol + columnLeftTolerance, startRow + (height * i) + rowUpTolerance),
				  Point(startCol + width - columnRightTolerance, startRow + (height * (i + 1)) - rowDownTolerance), RED, 1);
	}

#if CREATING_DEBUG == 1
	anImage.displayAndWait();
	anImage.saveTo("./" + anImage.getName() + "analyzed");
#endif

	writeResults(resultsFile, presences);
}

void ImageManipulator::writeResults(string file, vector<bool> presences) const throw(FileException) {
	ofstream outputStream(file.data(), ios::out);

	if (outputStream.is_open() == false)
		throw FileException(file.data());


	for (unsigned i = 0; i < presences.size(); ++i)
		if (presences[i] == true)
			outputStream << "presente" << endl;
		else
			outputStream << "assente" << endl;
	outputStream.close();
}

Image ImageManipulator::warpImagePerspective(vector<Point2f>& scannedPoints,
		vector<Point2f>& originalPoints, Image& scannedImage) {

	Mat transformMatrix = getPerspectiveTransform(scannedPoints, originalPoints);
	Mat outputMatrix;

	// SI CALCOLA LE DIMENSIONE DELLA NUOVA IMMAGINE A PARTIRE DAI PUNTI DELLA TABELLA DI QUELLA GIA' NOTA
	// VIENE LASCIATO ANCHE UN MARGINE D'ERRORE PER SICUREZZA
	warpPerspective(scannedImage.getImage(), outputMatrix, transformMatrix,
			Size(documentPointsCoordinates[2].x + 100, documentPointsCoordinates[2].y + 50));
//	warpPerspective(scannedImage.getImage(), outputMatrix, transformMatrix, Size(1000, 1000));

	Image imageOutput = Image(outputMatrix, scannedImage.getName() + "warped");
	return imageOutput;
}

Image ImageManipulator::alignImage(string originalPath, string toAlignPath, unsigned dilateErodeIteration)
throw (ContoursException, FileException) {
	Image original(originalPath.data(), "original");
	Image scannedImage(toAlignPath.data(), "scanned");
	Image scannedClone(scannedImage.cloneImage());

#if ANALYSIS_DEBUG == 1
	original.displayAndWait();
	scannedImage.displayAndWait();
	original.saveTo("./" + original.getName() + "preDilatedErodedBinary");
	scannedImage.saveTo("./" + scannedImage.getName() + "preDilatedErodedBinary");
#endif

	original.dilateAndErode(dilateErodeIteration);
	scannedImage.dilateAndErode(dilateErodeIteration);

	original.binaryTransform();
	scannedImage.binaryTransform();

	vector<vector<Point2f> > origContours = original.getContours();
	vector<vector<Point2f> > scannedContours = scannedImage.getContours();

	// SE SI TROVANO PIU' DI 4 CONTORNI PER IMMAGINE VUOL DIRE CHE LA DILATAZIONE E L'EROSIONE NON E'
	// AVVENUTA CORRETTAMENTE
	if (origContours.size() != 4 || scannedContours.size() != 4)
		throw ContoursException();

	vector<Square*> origSquares = SortAlgorithms::extrapulateSquareContours(origContours);
	vector<Square*> scannedSquares = SortAlgorithms::extrapulateSquareContours(scannedContours);

	// TODO FORSE BISOGNA CONTROLLARE CHE I CONTORNI DEL QUADRATO SIANO COSI' APPROSSIMATI DA TROVARE
	// SOLO 4 VERTICI PER OGNUNO. SOLLEVARE UN'ECCEZIONE IN CASO CONTRARIO
	SortAlgorithms::sortInternalSquaresPoints(origSquares);
	SortAlgorithms::sortInternalSquaresPoints(scannedSquares);
	SortAlgorithms::sortSquares(origSquares);
	SortAlgorithms::sortSquares(scannedSquares);

	vector<Point2f> origDocPoints = original.getDocumentVertices(origSquares);
	vector<Point2f> scannedDocPoints = scannedImage.getDocumentVertices(scannedSquares);

	// ASSUMENDO CHE I DUE VETTORI ABBIANO SEMPRE LA STESSA DIMENSIONE
	// ALTRIMENTI NEANCHE IL RICONOSCIMENTO FUNZIONA
	for (unsigned i = 0; i < origSquares.size(); i++) {
		delete origSquares[i];
		delete scannedSquares[i];
	}

	/* SALVA LE COORDINATE DEI PUNTI DEL FOGLIO IN MODO TALE DA POTERLI RIUTILIZZARE PER LA SCANSIONE */
	this->documentPointsCoordinates = origDocPoints;
	Image warped = this->warpImagePerspective(scannedDocPoints, origDocPoints, scannedClone);

#if ANALYSIS_DEBUG == 1
	original.displayAndWait();
	scannedImage.displayAndWait();
	warped.displayAndWait();
	original.saveTo("./" + original.getName() + "postDilatedErodedBinary");
	scannedImage.saveTo("./" + scannedImage.getName() + "postDilatedErodedBinary");
	warped.saveTo("./" + warped.getName() + "aligned");
#endif

	return Image(warped.getImage(), "warpedImage");
}


vector<Point2f> ImageManipulator::getDocumentPointsCoordinates() const {
	return documentPointsCoordinates;
}
