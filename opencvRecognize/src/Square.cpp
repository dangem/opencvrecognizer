#include "Square.hpp"

Square::Square(vector<Point2f> points)
{
	first = points[0];
	second = points[1];
	third = points[2];
	fourth = points[3];
}

int Square::getXSum() const
{
	return first.x + second.x + third.x + fourth.x;
}

int Square::getYSum() const
{
	return first.y + second.y + third.y + fourth.y;
}

void Square::print() const
{
	cout << first << second << third << fourth << endl;
}
