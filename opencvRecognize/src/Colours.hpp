#ifndef COLOURS_HPP_
#define COLOURS_HPP_

#define RED Scalar(0, 0, 255, 0)
#define GREEN Scalar(0, 255, 0, 0)
#define BLUE Scalar(255, 0, 0, 0)
#define WHITE Scalar(255, 255, 255, 0)
#define BLACK Scalar(0, 0, 0, 0)

#endif /* COLOURS_HPP_ */
