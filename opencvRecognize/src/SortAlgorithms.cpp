#include "SortAlgorithms.hpp"

SortAlgorithms::SortAlgorithms() {
}

template <typename T>
void SortAlgorithms::bubbleSort(vector<Comparable<T> >& comparables) {
	for (unsigned i = 0; i < comparables.size(); i++)
		for (unsigned j = 0; j < comparables.size()-1; j++)
			if (comparables[j] >= comparables[j+1]) {
				Comparable<T> tmp = comparables[j];
				comparables[j] = comparables[j+1];
				comparables[j+1] = tmp;
			}
}

void SortAlgorithms::sortSquares(vector<Square*>& squares) {
	vector<Comparable<Square*> > vecX;
	for (unsigned i = 0; i < squares.size(); i++)
		vecX.push_back(Comparable<Square*>(squares[i], squares[i]->getXSum()));

	SortAlgorithms::bubbleSort<Square*>(vecX);

	if (vecX[3].getData()->getYSum() > vecX[2].getData()->getYSum()) {
		squares[2] = vecX[3].getData();
		squares[1] = vecX[2].getData();
	}
	else {
		squares[1] = vecX[3].getData();
		squares[2] = vecX[2].getData();
	}

	if (vecX[0].getData()->getYSum() > vecX[1].getData()->getYSum()) {
		squares[3] = vecX[0].getData();
		squares[0] = vecX[1].getData();
	}
	else {
		squares[0] = vecX[0].getData();
		squares[3] = vecX[1].getData();
	}
}

void SortAlgorithms::sortInternalSquarePoints(Square* square) {
	vector<Comparable<Point2f> > vecX;
	vecX.push_back(Comparable<Point2f>(square->first, square->first.x));
	vecX.push_back(Comparable<Point2f>(square->second, square->second.x));
	vecX.push_back(Comparable<Point2f>(square->third, square->third.x));
	vecX.push_back(Comparable<Point2f>(square->fourth, square->fourth.x));

	SortAlgorithms::bubbleSort<Point2f>(vecX);

	if (vecX[3].getData().y > vecX[2].getData().y) {
		square->third = vecX[3].getData();
		square->second = vecX[2].getData();
	}
	else {
		square->third = vecX[2].getData();
		square->second = vecX[3].getData();
	}

	if (vecX[0].getData().y > vecX[1].getData().y) {
		square->fourth = vecX[0].getData();
		square->first = vecX[1].getData();
	}
	else {
		square->fourth = vecX[1].getData();
		square->first = vecX[0].getData();
	}
}

vector<Square*> SortAlgorithms::extrapulateSquareContours(vector<vector<Point2f> > contours) {
	vector<Square*> squares;

	for (unsigned i = 0; i < contours.size(); i++)
		squares.push_back(new Square(contours[i]));

	return squares;
}

void SortAlgorithms::sortInternalSquaresPoints(vector<Square*>& squares) {
	for (unsigned i = 0; i < squares.size(); i++)
		SortAlgorithms::sortInternalSquarePoints(squares[i]);
}
