#ifndef STUDENT_HPP_
#define STUDENT_HPP_

#include <string>
using namespace std;


class Student {
public:
	Student(string credentials, string id);
	Student(const Student& copy);

	string getCredentials() const;
	string getID() const;
private:
	string credentials;
	string id;
};

#endif /* STUDENT_HPP_ */



