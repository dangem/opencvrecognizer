#ifndef STUDENTQUANTITYEXCEPTION_HPP_
#define STUDENTQUANTITYEXCEPTION_HPP_

#include <exception>
using namespace std;


class StudentQuantityException : public exception {

public:
	const char* what() const throw() {
		return "Il numero minimo di studenti per creare un foglio di frequenza e' 3";
	}

};


#endif /* STUDENTQUANTITYEXCEPTION_HPP_ */
