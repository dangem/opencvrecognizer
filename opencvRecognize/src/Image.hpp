#ifndef IMAGE_HPP_
#define IMAGE_HPP_

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cstring>
#include <vector>
#include <fstream>
#include "Square.hpp"
#include "FileException.hpp"
using namespace cv;
using namespace std;

class Image {

public:
	Image(Mat matrixImage, string pathToImage, string name);
	Image(string pathToImage, string name) throw (FileException);
	Image(Mat matrixImage, string name);
	Image(const Image& image);

	Mat getImage() const;
	void setImage(Mat image);
	string getName() const;
	void display() const;
	void displayAndWait() const;
	Image cloneImage() const;
	vector<Point2f> getDocumentVertices(vector<Square*>& squaresVertices);
	vector<vector<Point2f> > getContours();
	void binaryTransform();
	void binaryTransform(int thresholdValue);
	void dilateAndErode(unsigned times);
	void saveTo(string path = "null") const throw (FileException);

	Image& operator=(const Image& image);

private:
	Mat matrix;
	string pathToImage;
	string name;

};


#endif /* IMAGE_HPP_ */
