#include "ImageManipulator.hpp"
#include <fstream>
using namespace std;

int main(int argc, char **argv) {

	ImageManipulator manipulator;

	// SI VUOLE ANALIZZARE UN'IMMAGINE
	if (argc == 5) {
		try {
			unsigned dilateErodeIteration = atoi(argv[4]);
			// PARAMETRI: IMMAGINE DI CONFRONTO, IMMAGINE SCANSIONATA (NON ALLINEATA),
			// NUMERO DI ITERAZIONI DI DILATAZIONE ED EROSIONE
			Image warpedImage = manipulator.alignImage(argv[1], argv[2], dilateErodeIteration);
			// PARAMETRO: PERCORSO E NOME FILE NEL QUALE SALVARE I RISULTATI DELL'ANALISI
			manipulator.analyzeImage(warpedImage, argv[3]);
		}
		catch (exception& c) {
			cerr << c.what() << endl;
			return 1;
		}
	}
	// SI VUOLE CREARE UN'IMMAGINE
	else if (argc == 3) {
		try {
			// PARAMETRO: PERCORSO FILE DAL QUALE LEGGERE I DATI PER CREARE L'IMMAGINE
			// E PERCORSO CON NOME FILE (SENZA ESTENSIONE) DELL'IMMAGINE CREATA
			manipulator.createImage(argv[1], argv[2]);
		}
		catch (exception& c) {
			cerr << c.what() << endl;
			return 1;
		}
	}
	else {
		cerr << "Numero inaspettato di argomenti da linea di comando" << endl;
	}

	return 0;
}
