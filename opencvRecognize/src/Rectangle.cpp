#include "Rectangle.hpp"

Rectangle::Rectangle(Size size)
	: size(size) {
}

Rectangle::Rectangle(Rectangle& copy)
	: size(copy.size) {
}

Size Rectangle::getSize() const {
	return size;
}

void Rectangle::drawOnImage(Point drawPoint, Mat image, Scalar color) {
	Point endPoint(drawPoint.x+size.width, drawPoint.y+size.height);
	rectangle(image, drawPoint, endPoint, color);
}

void Rectangle::drawOnImage(Point drawPoint, Mat image) {
	Rectangle::drawOnImage(drawPoint, image, Scalar(0, 0, 0, 0));
}

