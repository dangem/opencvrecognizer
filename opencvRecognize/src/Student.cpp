#include "Student.hpp"

Student::Student(string credentials, string id)
	: credentials(credentials), id(id) {
}

Student::Student(const Student& copy)
	: credentials(copy.credentials), id(copy.id) {
}

string Student::getCredentials() const {
	return credentials;
}

string Student::getID() const {
	return id;
}

