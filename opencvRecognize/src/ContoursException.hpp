#ifndef CONTOURSEXCEPTION_HPP_
#define CONTOURSEXCEPTION_HPP_

#include <exception>
using namespace std;


class ContoursException : public exception {

public:
	const char* what() const throw() {
		return "Trovati un numero di contorni diverso da 5";
	}

};

#endif /* CONTOURSEXCEPTION_HPP_ */
