#include "Image.hpp"
#include <iostream>
using namespace std;

Image::Image(Mat matrixImage, string pathToImage, string name)
	: matrix(matrixImage), pathToImage(pathToImage), name(name) {
}

Image::Image(string pathToImage, string name) throw (FileException)
	: pathToImage(pathToImage), name(name) {

	ifstream fileExistence(pathToImage.data(), ios::in);
	if (fileExistence.is_open() == false)
		throw FileException(pathToImage.data());
	this->matrix = imread(pathToImage);
}

Image::Image(const Image& image)
	: matrix(image.matrix), pathToImage(image.pathToImage), name(image.name) {
}

Image::Image(Mat matrixImage, string name)
	: matrix(matrixImage), pathToImage("null"), name(name) {
}

Mat Image::getImage() const {
	return this->matrix;
}

void Image::setImage(Mat image) {
	this->matrix = image;
}

string Image::getName() const {
	return this->name;
}

Image Image::cloneImage() const {
	return Image(this->getImage().clone(), this->pathToImage, this->name + "clone");
}

void Image::dilateAndErode(unsigned times) {
	for (unsigned i = 0; i < times; i++)
		dilate(matrix, matrix, Mat());

	for (unsigned i = 0; i < times; i++)
		erode(matrix, matrix, Mat());
}

void Image::binaryTransform() {
	binaryTransform(128);
}

void Image::binaryTransform(int thresholdValue) {
	cvtColor(matrix, matrix, CV_RGB2GRAY);
	threshold(matrix, matrix, thresholdValue, 255, THRESH_BINARY);
}

vector<vector<Point2f> > Image::getContours() {
	vector<vector<Point> > rawContours;
	Mat matrixClone = matrix.clone();
	findContours(matrixClone, rawContours, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

	// SE TUTTO E' ANDATO BENE ALLORA SI DOVREBBERO TROVARE 5 CONTORNI: IL PRIMO RIGUARDA LA PARTE ESTERNA DEL FOGLIO
	// MENTRE TUTTI GLI ALTRI RIGUARDANO I QUADRATI NERI DI RIFERIMENTO
	vector<vector<Point> > approxContours;
	for (unsigned i = 1; i <= 4; i++) {
		vector<Point> tmp;
		// APPROSSIMA I CONTORNI TROVATI IN MODO TALE DA AVERE SOLO 4 VERTICI PER OGNI QUADRATO NERO
		approxPolyDP(rawContours[i], tmp, 5, true);
		approxContours.push_back(tmp);
	}

	// CONVERTE IL PRECEDENTE vector<vector<Point>> IN vector<vector<Point2f>>
	vector<vector<Point2f> > vertices;
	for (unsigned i = 0; i < approxContours.size(); i++) {
		vector<Point2f> tmp;

		for (unsigned j = 0; j < approxContours[i].size(); j++)
			tmp.push_back(Point2f(approxContours[i][j].x, approxContours[i][j].y));

		vertices.push_back(tmp);
	}

	return vertices;
}

vector<Point2f> Image::getDocumentVertices(vector<Square*>& squaresVertices) {
	vector<Point2f> docVertices;

	docVertices.push_back(squaresVertices[0]->second);
	docVertices.push_back(squaresVertices[1]->first);
	docVertices.push_back(squaresVertices[2]->fourth);
	docVertices.push_back(squaresVertices[3]->third);

	return docVertices;
}

void Image::display() const {
	namedWindow(this->name, CV_WINDOW_AUTOSIZE);
	imshow(this->name, this->matrix);
}

void Image::displayAndWait() const {
	display();
	waitKey();
}

Image& Image::operator=(const Image& image) {
	this->matrix = image.matrix;
	this->name = image.name;
	this->pathToImage = image.pathToImage;
	return (*this);
}

void Image::saveTo(string path) const throw (FileException) {
	vector<int> imwriteParam;
	imwriteParam.push_back(CV_IMWRITE_PNG_COMPRESSION);
	string fileExtension = ".png";
	if (path != "null")
		// IL PATH DEVE ESSERE GIA' COMPRENSIVO DI NOME DEL FILE MA NON DELL'ESTENSIONE
		imwrite((path + fileExtension).data(), this->matrix, imwriteParam);
	else
		imwrite(("./" + this->name + fileExtension).data(), this->matrix, imwriteParam);


	ifstream fileExistence((path + fileExtension).data(), ios::in);
	if (fileExistence.is_open() == false)
		throw FileException(path.data());
}
