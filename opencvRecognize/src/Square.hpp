#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <vector>
#include <iostream>

using namespace std;
using namespace cv;

class Square
{
public:
	Square(vector<Point2f> points);

	int getXSum() const;
	int getYSum() const;

	void print() const;

	Point2f first;
	Point2f second;
	Point2f third;
	Point2f fourth;
};

#endif /* SQUARE_HPP_ */
